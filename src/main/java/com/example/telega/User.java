package com.example.telega;

import lombok.Data;

@Data
public class User {
    private String uuid;
    private String name;
}
