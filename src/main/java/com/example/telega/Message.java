package com.example.telega;

import lombok.Data;

@Data
public class Message {
    private String uuid;
    private String text;
    private String userId;
    private String groupId;
}
