package com.example.telega;

import java.util.List;

public interface MessageDao {
    String create(Message message);

    void delete(String uuid);

    List<Message> fetchAll();

    List<Message> fetchByUserId(String userId);

    List<Message> fetchByGroupId(String groupId);
}
