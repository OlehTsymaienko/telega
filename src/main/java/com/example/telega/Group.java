package com.example.telega;

import lombok.Data;

@Data
public class Group {
    private String  uuid;
    private String name;

}
