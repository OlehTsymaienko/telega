package com.example.telega;

import lombok.Data;

@Data
public class User2Group {
    private String groupUuid;
    private String userUuid;
}
