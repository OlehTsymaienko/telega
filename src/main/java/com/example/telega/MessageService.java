package com.example.telega;

import java.util.List;

public interface MessageService {
    String add(String text, String groupId, String userId);
    List<Message> fetchAll();
    List<Message> fetchByGroupId(String groupId);
    List<Message> fetchByUserId(String userId);
}
