package com.example.telega;

import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {
    private MessageDao messageDao;
    @Override
    public String add(String text, String groupId, String userId) {
        Message message = new Message();
        message.setGroupId(groupId);
        message.setUserId(userId);
        message.setText(text);
        return messageDao.create(message);
    }

    @Override
    public List<Message> fetchAll() {
        return null;
    }

    @Override
    public List<Message> fetchByGroupId(String groupId) {
        return null;
    }

    @Override
    public List<Message> fetchByUserId(String userId) {
        return null;
    }
}
