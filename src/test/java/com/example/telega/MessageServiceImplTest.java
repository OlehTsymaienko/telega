package com.example.telega;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MessageServiceImplTest {

    @Autowired
    private  MessageService messageService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void add() {
        messageService.add("msg", "grp1", "usr1");
    }

    @Test
    void fetchAll() {
    }

    @Test
    void fetchByGroupId() {
    }

    @Test
    void fetchByUserId() {
    }
}